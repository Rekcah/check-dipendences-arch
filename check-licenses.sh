#!/bin/bash

# Read the package name from the input
if [ -z "$1" ]; then
    echo "Usage: $0 package-name"
    exit 1
fi
package=$1

# Check if the package is installed
if ! pacman -Qq "$package" >/dev/null 2>&1; then
    echo "Package not found: $package"
    exit 1
fi

# Execute pactree for the package and save it in "deps" variable
deps=$(pactree -u "$package")

# Extract licenses from dependences with sed
deps_with_licenses=$(echo "$deps" | xargs pacman -Qi | sed -E -n 's/^Name\s*:\s+(.*)$/\1/p; s/Licenses\s*?:\s+(.+)/\1/p')

# Print only package's names and licenses
echo "$deps_with_licenses" | while read -r line; do
  # If the rows start with package name, print the name
  if [[ "$deps" == *"$line"* ]]; then
    echo -n "$line: "
    echo
  # Otherwise print the license
  else
    if echo "$line" | grep -qiE '(gpl|gnu)'; then
        echo -en "$line\n\n"
    else
        echo -en "\033[31m$line\033[0m\n\n"
    fi
  fi
done

